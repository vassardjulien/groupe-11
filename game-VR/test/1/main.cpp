#include <SFML/Graphics.hpp>
#define TAILLE_PERSO 50
#define VITESSE_PERSO 5
#define VITESSE_MISSILE 10
#define HAUTEUR_FENETRE
#define TAILLE_MISSILE 40

using namespace sf;

typedef struct
{
    int x;
    int y;
} Cord;

typedef struct
{
    Cord c;
} Perso1;

typedef struct
{
    Cord c;
} Perso2;

typedef struct
{
    Cord c;
} Missile1;

int main()
{
    int missileAvance=0;
    Perso1 p1;
    Perso2 p2;
    Missile1 m1;
    p1.c.x=0;
    p1.c.y=350;

    p2.c.x=1400;
    p2.c.y=350;

    //   ml.c.x=
    // ml.c.y=

    RenderWindow app(VideoMode(1500, 800), "SFML window");

    CircleShape perso1(TAILLE_PERSO);
    CircleShape perso2(TAILLE_PERSO);
    CircleShape missile1(TAILLE_MISSILE);


    perso2.setFillColor(Color::Red);
    perso1.setFillColor(Color::Cyan);
    missile1.setFillColor(Color::Cyan);


    perso1.setPosition(p1.c.x, p1.c.y);
    perso2.setPosition(p2.c.x, p2.c.y);





    Texture texture;


    while (app.isOpen())
    {

        Event event;
        while (app.pollEvent(event))
        {

            if (Keyboard::isKeyPressed(Keyboard::Z))
                p1.c.y-=VITESSE_PERSO;
            else if (Keyboard::isKeyPressed(Keyboard::S))
                p1.c.y+=VITESSE_PERSO;
            perso1.setPosition(p1.c.x, p1.c.y);


            if (Keyboard::isKeyPressed(Keyboard::P))
                p2.c.y-=VITESSE_PERSO;
            else if (Keyboard::isKeyPressed(Keyboard::M))
                p2.c.y+=VITESSE_PERSO;
            perso2.setPosition(p2.c.x, p2.c.y);



            if (Keyboard::isKeyPressed(Keyboard::E))
            {
                missileAvance=1;
            }






            if (event.type == Event::Closed)
                app.close();
        }
        if (missileAvance>=1)
        {
            app.clear();
            missileAvance+=1;

            app.draw(missile1);
            app.display();
        }
        missile1.setPosition(((p1.c.x)+100)+missileAvance, (p1.c.y)+10);

        app.clear();

        app.draw(perso1);
        app.draw(perso2);






        app.display();
    }
    return EXIT_SUCCESS;
}




