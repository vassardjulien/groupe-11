#include <SFML/Graphics.hpp>
#define LARGEUR_FENETRE 1500
#define HAUTEUR_FENETRE 800
#define CHOIX_PERSO "choixperso.png"
#define CHOIX_MAP "choixmap_v2.png"

using namespace sf;
using namespace std;

int main()
{
    int PositionLargeurJouer = LARGEUR_FENETRE*1.8/4;
    int PositionHauteurJouer = HAUTEUR_FENETRE*2/4;
    int PositionLargeurQuitter = LARGEUR_FENETRE*1.78/4;
    int PositionHauteurQuitter = HAUTEUR_FENETRE*2/3;
    int PositionLargeurRetour = 125;
    int PositionHauteurRetour = 625;
    int PositionLargeurSuivant = 1200;
    int PositionHauteurSuivant = 625;
    int PositionLargeurPerso1 = 120;
    int PositionHauteurPerso1 = 180;
    int PositionLargeurPerso2 = 380;
    int PositionHauteurPerso2 = 170;
    int PositionLargeurPerso3 = 865;
    int PositionHauteurPerso3 = 160;
    int PositionLargeurPerso4 = 1190;
    int PositionHauteurPerso4 = 160;
    int PositionLargeurPerso5 = 500;
    int PositionHauteurPerso5 = 485;
    int PositionLargeurPerso6 = 750;
    int PositionHauteurPerso6 = 470;


    int MenuPrincipal = 1;
    int MenuPerso = 0;
    int MenuCarte = 0;
    int MenuConfirme = 0;


    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Battle Shooter");

    Font Police;
    if (!Police.loadFromFile("arial.ttf"))
    {
        printf("Erreur police !");
    }

    Text Nomjeu("BATTLE SHOOTER", Police, 70);
    Nomjeu.setStyle(Text::Bold);
    Nomjeu.setPosition(LARGEUR_FENETRE*0.9/3,HAUTEUR_FENETRE*0.5/3);
    Nomjeu.setColor(Color :: Cyan);

    Text Jouer("Jouer", Police, 40);
    Jouer.setPosition(PositionLargeurJouer,PositionHauteurJouer);
    Jouer.setColor(Color :: White);

    Text Quitter("Quitter", Police, 40);
    Quitter.setPosition(PositionLargeurQuitter,PositionHauteurQuitter);
    Quitter.setColor(Color :: White);

    RectangleShape carre;
    carre.setSize(Vector2f(125,50));
    carre.setFillColor(Color :: White);
    carre.setPosition(PositionLargeurRetour,PositionHauteurRetour);



    app.clear();
    app.draw(Nomjeu);
    app.draw(Jouer);
    app.draw(Quitter);
    app.display();


    while (app.isOpen())
    {

        Event event;
        while (app.pollEvent(event))
        {

            Vector2i SourisPos = Mouse::getPosition(app);

            if (MenuPrincipal >=1)
            {
                if ((((SourisPos.x >= PositionLargeurQuitter && SourisPos.x <= PositionLargeurQuitter+125) && (SourisPos.y >= PositionHauteurQuitter && SourisPos.y <= PositionHauteurQuitter+50)) && Mouse::isButtonPressed(Mouse::Left)) || (event.key.code == Keyboard :: Escape))
                {
                    app.close();
                }
                //Switch menu perso
                if (((SourisPos.x >= PositionLargeurJouer && SourisPos.x <= PositionLargeurJouer+200) && (SourisPos.y >= PositionHauteurJouer && SourisPos.y <= PositionHauteurJouer+50)) && Mouse::isButtonPressed(Mouse::Left))//event.key.code == Keyboard::Return
                {
                    Texture choix;
                    if (!choix.loadFromFile(CHOIX_PERSO))
                        printf("PB de chargement de l'image %s !\n", CHOIX_PERSO);
                    Sprite perso;
                    perso.setTexture(choix);

                    app.draw(perso);
                    app.display();
                    MenuPerso += 1;
                    MenuPrincipal -=1;
                }
            }

            else if (MenuPerso >= 1)
            {
                //Revenir au menu principal
                if ((SourisPos.x >= PositionLargeurRetour && SourisPos.x <= PositionLargeurRetour+200) && (SourisPos.y >= PositionHauteurRetour && SourisPos.y <= PositionHauteurRetour+50) && Mouse::isButtonPressed(Mouse::Left))
                {
                    MenuPrincipal += 1;
                    app.clear();
                    app.draw(Nomjeu);
                    app.draw(Jouer);
                    app.draw(Quitter);
                    app.display();
                }
                //Switch menu carte
                if ((SourisPos.x >= PositionLargeurSuivant && SourisPos.x <= PositionLargeurSuivant+200) && (SourisPos.y >= PositionHauteurSuivant && SourisPos.y <= PositionHauteurSuivant+50) && Mouse::isButtonPressed(Mouse::Left))
                {
                    MenuCarte += 1;
                    MenuPerso -= 1;
                    Texture map1;
                    if (!map1.loadFromFile(CHOIX_MAP))
                        printf("PB de chargement de l'image %s !\n", CHOIX_MAP);
                    Sprite carte;
                    carte.setTexture(map1);

                    app.draw(carte);
                    app.display();
                }

                if ((SourisPos.x >= PositionLargeurPerso1 && SourisPos.x <= PositionLargeurPerso1+170) && (SourisPos.y >= PositionHauteurPerso1 && SourisPos.y <= PositionHauteurPerso1+300) && Mouse::isButtonPressed(Mouse::Left))
                {
                    printf("Perso1\n");
                }

                if ((SourisPos.x >= PositionLargeurPerso2 && SourisPos.x <= PositionLargeurPerso2+170) && (SourisPos.y >= PositionHauteurPerso2 && SourisPos.y <= PositionHauteurPerso2+300) && Mouse::isButtonPressed(Mouse::Left))
                {
                    printf("Perso2\n");
                }

                if ((SourisPos.x >= PositionLargeurPerso3 && SourisPos.x <= PositionLargeurPerso3+265) && (SourisPos.y >= PositionHauteurPerso3 && SourisPos.y <= PositionHauteurPerso3+300) && Mouse::isButtonPressed(Mouse::Left))
                {
                    printf("Perso3\n");
                }

                if ((SourisPos.x >= PositionLargeurPerso4 && SourisPos.x <= PositionLargeurPerso4+220) && (SourisPos.y >= PositionHauteurPerso4 && SourisPos.y <= PositionHauteurPerso4+300) && Mouse::isButtonPressed(Mouse::Left))
                {
                    printf("Perso4\n");
                }

                if ((SourisPos.x >= PositionLargeurPerso5 && SourisPos.x <= PositionLargeurPerso5+180) && (SourisPos.y >= PositionHauteurPerso5 && SourisPos.y <= PositionHauteurPerso5+275) && Mouse::isButtonPressed(Mouse::Left))
                {
                    printf("Perso5\n");
                }

                if ((SourisPos.x >= PositionLargeurPerso6 && SourisPos.x <= PositionLargeurPerso6+225) && (SourisPos.y >= PositionHauteurPerso6 && SourisPos.y <= PositionHauteurPerso6+310) && Mouse::isButtonPressed(Mouse::Left))
                {
                    printf("Perso6\n");
                }

            }

            else if (MenuCarte >= 1)
            {
                //Revenir au menu perso
                if ((SourisPos.x >= PositionLargeurRetour && SourisPos.x <= PositionLargeurRetour+200) && (SourisPos.y >= PositionHauteurRetour && SourisPos.y <= PositionHauteurRetour+50) && Mouse::isButtonPressed(Mouse::Left))
                {
                    MenuCarte -= 1;
                    MenuPerso += 1;
                    Texture choix;
                    if (!choix.loadFromFile(CHOIX_PERSO))
                        printf("PB de chargement de l'image %s !\n", CHOIX_PERSO);
                    Sprite perso;
                    perso.setTexture(choix);

                    app.draw(perso);
                    app.display();
                }

                if ((SourisPos.x >= PositionLargeurSuivant && SourisPos.x <= PositionLargeurSuivant+200) && (SourisPos.y >= PositionHauteurSuivant && SourisPos.y <= PositionHauteurSuivant+50) && Mouse::isButtonPressed(Mouse::Left))
                {
                    MenuCarte -= 1;
                    //MenuConfirme += 1;
                    app.clear(Color :: White);
                    app.display();
                }

               // app.draw(carre);
                //app.display();

            }
        }

    }

    return EXIT_SUCCESS;
}
