#include <SFML/Graphics.hpp>

#define LARGEUR_FEN 1500
#define HAUTEUR_FEN 800

#define FOND1 "foot.jpg"
#define FOND2 "nuage.jpg"
#define FOND3 "ville.jpg"


#define NOM_IMAGE1 "ronaldo.png"
#define NOM_IMAGE2 "messi.png"
#define NOM_IMAGE3 "naruto.png"
#define NOM_IMAGE4 "dbz.png"
#define NOM_IMAGE5 "wolverine.png"
#define NOM_IMAGE6 "hulk.png"

#define BOULE1 "feu.png"
#define BOULE2 "eau.png"
#define BOULE3 "herbe.png"

using namespace sf;

void affichePersonnage (RenderWindow &fenetre);
void afficheFond (RenderWindow &fenetre);
void afficheBoule (RenderWindow &fenetre);


int main()
{
    RenderWindow fenetre(VideoMode(LARGEUR_FEN, HAUTEUR_FEN), "JEU");
    afficheFond (fenetre);
    affichePersonnage (fenetre);
    afficheBoule (fenetre);
    fenetre.display();

    system("PAUSE");

}

void affichePersonnage (RenderWindow &fenetre)
{


    Texture image1;
    Texture image2;
    Texture image3;
    Texture image4;
    Texture image5;
    Texture image6;

    if (!image1.loadFromFile(NOM_IMAGE1))
        printf("PB de chargement de l'image %s !\n", NOM_IMAGE1);
    if (!image2.loadFromFile(NOM_IMAGE2))
        printf("PB de chargement de l'image %s !\n", NOM_IMAGE2);
    if (!image3.loadFromFile(NOM_IMAGE3))
        printf("PB de chargement de l'image %s !\n", NOM_IMAGE3);
    if (!image4.loadFromFile(NOM_IMAGE4))
        printf("PB de chargement de l'image %s !\n", NOM_IMAGE4);
    if (!image5.loadFromFile(NOM_IMAGE5))
        printf("PB de chargement de l'image %s !\n", NOM_IMAGE5);
    if (!image6.loadFromFile(NOM_IMAGE6))
        printf("PB de chargement de l'image %s !\n", NOM_IMAGE6);

    Sprite ronaldo;
    Sprite messi;
    Sprite naruto;
    Sprite dbz;
    Sprite wolverine;
    Sprite hulk;

    ronaldo.setTexture(image1);
    messi.setTexture(image2);
    naruto.setTexture(image3);
    dbz.setTexture(image4);
    wolverine.setTexture(image5);
    hulk.setTexture(image6);

    ronaldo.setPosition(0,0);
    messi.setPosition(200,0);
    naruto.setPosition(400,0);
    dbz.setPosition(600,0);
    wolverine.setPosition(800,0);
    hulk.setPosition(1000,0);

    /*fenetre.draw(ronaldo);
    fenetre.draw(messi);
    fenetre.draw(naruto);
    fenetre.draw(dbz);*/
    fenetre.draw(wolverine);
    fenetre.draw(hulk);



}

void afficheFond(RenderWindow &fenetre)
{


    Texture fond1;
    Texture fond2;
    Texture fond3;

    if (!fond1.loadFromFile(FOND1))
        printf("PB de chargement de l'image %s !\n", FOND1);
    if (!fond2.loadFromFile(FOND2))
        printf("PB de chargement de l'image %s !\n", FOND2);
    if (!fond3.loadFromFile(FOND3))
        printf("PB de chargement de l'image %s !\n", FOND3);

    Sprite foot;
    Sprite nuage;
    Sprite ville;

    foot.setTexture(fond1);
    nuage.setTexture(fond2);
    ville.setTexture(fond3);

    fenetre.draw(foot);
    //fenetre.draw(nuage);
    //fenetre.draw(ville);




}

void afficheBoule (RenderWindow &fenetre)
{
    Texture boule1;
    Texture boule2;
    Texture boule3;

    if (!boule1.loadFromFile(BOULE1))
        printf("PB de chargement de l'image %s !\n", BOULE1);
    if (!boule2.loadFromFile(BOULE2))
        printf("PB de chargement de l'image %s !\n", BOULE2);
    if (!boule3.loadFromFile(BOULE3))
        printf("PB de chargement de l'image %s !\n", BOULE3);

    Sprite feu;
    Sprite eau;
    Sprite herbe;

    feu.setTexture(boule1);
    eau.setTexture(boule2);
    herbe.setTexture(boule3);

    //fenetre.draw(feu);
    //fenetre.draw(eau);
    fenetre.draw(herbe);
}
