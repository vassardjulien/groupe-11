#include <SFML/Graphics.hpp>
#define TAILLE_PERSO 50
#define VITESSE_PERSO 5
#define VITESSE_MISSILE 1
#define HAUTEUR_FENETRE
#define TAILLE_MISSILE 40

using namespace sf;

typedef struct
{
    int x;
    int y;
} Cord;


int main()
{
    Cord p1, m1;
    int missileAvance=0;
    int missileMonte=0;
    int missileDescend=0;


    p1.x=0;
    p1.y=350;




    RenderWindow app(VideoMode(1500, 800), "SFML window");

    CircleShape perso1(TAILLE_PERSO);
    CircleShape perso2(TAILLE_PERSO);
    CircleShape missile1(TAILLE_MISSILE);


    perso2.setFillColor(Color::Red);
    perso1.setFillColor(Color::Cyan);
    missile1.setFillColor(Color::Cyan);


    perso1.setPosition(p1.x, p1.y);





    Texture texture;


    while (app.isOpen())
    {

        Event event;
        while (app.pollEvent(event))
        {

            if (Keyboard::isKeyPressed(Keyboard::Z))
                p1.y-=VITESSE_PERSO;
            else if (Keyboard::isKeyPressed(Keyboard::S))
                p1.y+=VITESSE_PERSO;
            perso1.setPosition(p1.x, p1.y);



            if (Keyboard::isKeyPressed(Keyboard::E))
            {
                missileDescend=0;
                missileMonte=0;
                missileAvance=1;
                m1.x=(p1.x)+100;
                m1.y=(p1.y)+10;
            }
             if (Keyboard::isKeyPressed(Keyboard::E) && Keyboard::isKeyPressed(Keyboard::Z))
            {
                missileDescend=0;
                missileMonte=1;
                missileAvance=1;
                m1.x=(p1.x)+100;
                m1.y=(p1.y)+10;

            }
            else if (Keyboard::isKeyPressed(Keyboard::E) && Keyboard::isKeyPressed(Keyboard::S))
            {
                missileMonte=0;
                missileDescend=1;
                missileAvance=1;
                m1.x=(p1.x)+100;
                m1.y=(p1.y)+10;
            }

            if (event.type == Event::Closed)
                app.close();

        }


        if (missileAvance==1 && missileDescend==0 && missileMonte==0)
        {
            m1.x+=VITESSE_MISSILE;

            if (m1.x+TAILLE_MISSILE>=1500)
               m1.x=m1.x;

            app.clear();
            app.draw(missile1);
            app.draw(perso1);
            app.display();

        }

        if (missileDescend==1 && missileAvance==1 && missileMonte==0)
        {
            m1.x+=VITESSE_MISSILE;
            m1.y+=VITESSE_MISSILE;

            if(m1.y+TAILLE_MISSILE>=800){
                 missileDescend=0;
                 missileMonte=1;
            }

        }
        if (missileMonte==1 && missileAvance==1 && missileDescend==0)
        {
            m1.x+=VITESSE_MISSILE;
            m1.y-=VITESSE_MISSILE;

            if(m1.y+TAILLE_MISSILE<=0){
                 missileDescend=1;
                 missileMonte=0;
            }

        }





        missile1.setPosition(m1.x, m1.y);

        app.clear();
        app.draw(missile1);
        app.draw(perso1);
        app.display();

    }
    return EXIT_SUCCESS;
}
