#include <SFML/Graphics.hpp>
#define TAILLE_PERSO 50
#define VITESSE_PERSO 5
#define HAUTEUR_FENETRE

using namespace sf;

typedef struct
{
    int x;
    int y;
} Cord;

typedef struct
{
    Cord c;
} Perso1;

typedef struct
{
    Cord c;
} Perso2;

int main()
{
Perso1 p1;
Perso2 p2;

 p1.c.x=TAILLE_PERSO/2;
 p1.c.y=TAILLE_PERSO/2;

 p2.c.x=TAILLE_PERSO/2;
 p2.c.y=TAILLE_PERSO/2;

    RenderWindow app(VideoMode(800, 600), "SFML window");

    CircleShape perso1(TAILLE_PERSO);
    CircleShape perso2(TAILLE_PERSO);
    perso2.setFillColor(Color::Red);
    perso1.setOrigin(TAILLE_PERSO/2,TAILLE_PERSO/2);
    perso2.setOrigin(TAILLE_PERSO/2,TAILLE_PERSO/2);
    perso1.setPosition(p1.c.x, p1.c.y);
    perso2.setPosition(800-(p2.c.x), p2.c.y);

   Texture texture;


    while (app.isOpen())
    {

        Event event;
        while (app.pollEvent(event))
        {
            if (Keyboard::isKeyPressed(Keyboard::Up))
                p1.c.y-=VITESSE_PERSO;
            else if (Keyboard::isKeyPressed(Keyboard::Down))
                p1.c.y+=VITESSE_PERSO;

            perso1.setPosition(p1.c.x, p1.c.y);

            if (Keyboard::isKeyPressed(Keyboard::Z))
                p2.c.y-=VITESSE_PERSO;
            else if (Keyboard::isKeyPressed(Keyboard::S))
                p2.c.y+=VITESSE_PERSO;

            perso2.setPosition(800-(p2.c.x), p2.c.y);
}

            if (event.type == Event::Closed)
                app.close();



        app.clear();

        app.draw(perso1);
        app.draw(perso2);

        app.display();
    }
        return EXIT_SUCCESS;
    }


