#include <SFML/Graphics.hpp>
using namespace sf;
int main()
{
    // Create the main window
    sf::RenderWindow app(sf::VideoMode(800, 600), "SFML window");

      srand(time(NULL));
      CircleShape missiles[3];
      int i;
      for (i=0;i<3; i++)
      {
          missiles[i].setRadius(20);
          missiles[i].setPosition( rand()%(800-20*2),0);

      }


    // Load a sprite to display

	// Start the game loop
    while (app.isOpen())
    {
        // Process events
        sf::Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                app.close();
        }

        // Clear screen
        app.clear();
        for (i=0;i<3; i++)
      {

          app.draw(missiles[i]);
          missiles[i].setPosition(missiles[i].getPosition().x, missiles[i].getPosition().y +1);
      }



        // Update the window
        app.display();
    }

    return EXIT_SUCCESS;
}
